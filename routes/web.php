<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('logout', 'Auth\LoginController@logout');


Route::get('/', 'HomeController@index')->name('home');

Route::get('/home', 'HomeController@index')->name('home-one');

Route::get('/home-two', 'HomeController@homeTwo')->name('home-two');

